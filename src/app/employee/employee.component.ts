import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { RouterModule, Routes, Router, NavigationExtras } from '@angular/router';

@Component({
    selector: 'employee-list',
    templateUrl: 'employee.component.html',
    styleUrls: ['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
    employees: any;
    constructor(private employeeService: EmployeeService, private router: Router) { }

    ngOnInit() {
        this.employeeService.getallemployees().subscribe(data => {
            this.employees = data;
        });

    }

    navigateToTimeSheet(item) {
        this.router.navigate(['timesheet', item.id]);
    }
}