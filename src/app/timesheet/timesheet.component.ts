import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { MatDialog, MatDialogConfig } from "@angular/material";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MyDialogComponent } from '../my-dialog/my-dialog.component';

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})
export class TimesheetComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private employeeService: EmployeeService, private dialog: MatDialog) { }

  public id: any;
  employees: any;
  selectedEmployee: any;
  addNewTimeSheetFlag: any;
  Sunday: any;
  Monday: any;
  Tuesday: any;
  Wednesday: any;
  Thursday: any;
  Friday: any;
  Saturday: any;
  tasks: any;
  data: any;
  selectedTask: any;
  Index: any;
  timesheets: any;

  ngOnInit() {
    this.Index = 0;
    this.route.params.subscribe(params => {
      this.id = params['employeeId'];
      console.log(this.id);
    });

    this.employeeService.getEmployeeTimeSheet(this.id, this.Index).subscribe(data => {
      this.timesheets = data;
    })

    this.employeeService.getallemployeesW().subscribe(data => {
      this.employees = data;
      console.log(this.id)
      for (var i = 0; i < this.employees.length; i++) {
        console.log(this.employees[i].id == this.id)
        if (this.employees[i].id == this.id) {
          this.selectedEmployee = this.employees[i]
          console.log(this.selectedEmployee);
        }
      }
    });

    this.employeeService.getalltasks().subscribe(data => {
      this.tasks = data;
    });

    this.Sunday = 0;
    this.Monday = 0;
    this.Tuesday = 0;
    this.Wednesday = 0;
    this.Thursday = 0;
    this.Friday = 0;
    this.Saturday = 0;

    this.addNewTimeSheetFlag = false;
  }

  PreviousIndex() {
    this.Index -= 1;
    this.employeeService.getEmployeeTimeSheet(this.id, this.Index).subscribe(data => {
      this.timesheets = data;
    })
  }

  NextIndex() {
    this.Index += 1;
    this.employeeService.getEmployeeTimeSheet(this.id, this.Index).subscribe(data => {
      this.timesheets = data;
    })
  }

  backToEmployee() {
    this.router.navigate(['employees']);
  }

  addNewTimeSheet() {
    this.addNewTimeSheetFlag = false;
    var data = {
      EmployeeId: this.selectedEmployee.id,
      TaskId: this.selectedTask.id,
      DayEffortLogs: [
        { DayName: "Sunday", Value: this.Sunday },
        { DayName: "Monday", Value: this.Monday },
        { DayName: "Tuesday", Value: this.Tuesday },
        { DayName: "Wednesday", Value: this.Wednesday },
        { DayName: "Thursday", Value: this.Thursday },
        { DayName: "Friday", Value: this.Friday },
        { DayName: "Saturday", Value: this.Saturday }
      ]
    }
    this.employeeService.addNewTimeSheet(data).subscribe(data => {
      console.log(data);
      this.data.push({
        task: {
          name: this.selectedTask.name
        },
        dayEffortLogs: [
          { DayName: "Sunday", value: this.Sunday },
          { DayName: "Monday", value: this.Monday },
          { DayName: "Tuesday", value: this.Tuesday },
          { DayName: "Wednesday", value: this.Wednesday },
          { DayName: "Thursday", value: this.Thursday },
          { DayName: "Friday", value: this.Friday },
          { DayName: "Saturday", value: this.Saturday }
        ]
      })
    });
  }

  changedEmployee() {
    this.Index = 0;
    this.employeeService.getEmployeeTimeSheet(this.selectedEmployee.id, 0).subscribe(data => {
      this.data = data;
    })
  }

  addingNew() {
    this.addNewTimeSheetFlag = true;
  }

  openDialog(): void {
    console.log(this.selectedEmployee)
    const dialogRef = this.dialog.open(MyDialogComponent, {
      width: '500px',
      data: { "tasks": this.tasks, "EmployeeId": this.selectedEmployee.id }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result)
      // this.animal = result;
      this.employeeService.getEmployeeTimeSheet(this.selectedEmployee.id, this.Index).subscribe(data => {
        this.data = data;
      }, err => {
        this.employeeService.updateEffort(result).subscribe(data => {
          console.log(data);
        })
      })
    });
  }

}
