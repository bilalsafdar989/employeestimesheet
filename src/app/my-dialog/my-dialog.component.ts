import { Component, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material'
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-my-dialog',
  templateUrl: './my-dialog.component.html',
  styleUrls: ['./my-dialog.component.scss']
})
export class MyDialogComponent implements OnInit {

  tasks: any;
  selectedTask: any;
  Effort: any;
  picker: any;
  date: any;

  constructor(public dialogRef: MatDialogRef<MyDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private employeeService: EmployeeService) { }

  ngOnInit() {
    console.log(this.data);
    this.tasks = this.data.tasks;
  }

  SaveEffort() {

    var data = {
      TaskId: this.selectedTask.id,
      Hours: Number(this.Effort),
      EmployeeId: Number(this.data.EmployeeId),
      CreatedDate: this.date
    }

    this.employeeService.addNewEffort(data).subscribe(data => {
      console.log(data);

    });

    this.dialogRef.close(data);
  }

}
